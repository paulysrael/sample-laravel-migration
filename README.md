# My project's README
# upgraded my PHP to version 5.6.4
# laravel 5.4.19

### create 2 tables using laravel migrate ###

1. Go to http://localhost/phpmyadmin
   - create database : 'sample_blog'
2. Open artisan cli
   - go to your projects directory and type : php artisan migrate

   - eq. C:\xampp\htdocs\my_project>php artisan migrate

3. Go to http://localhost/phpmyadmin
   - check created tables