<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('article_slug');
            $table->text('article_title');
            $table->text('article_blurb');
            $table->text('article_content');
            $table->tinyInteger('article_status');
            $table->timestamps();
            $table->integer('author_id')->unsigned();
            $table->foreign('author_id')->references('id')->on('authors');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
